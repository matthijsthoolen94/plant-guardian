FROM gitpod/workspace-full

USER gitpod

RUN sudo apt-get -q update && \
    sudo apt-get install ruby-full -y

# Install Ruby on Rails
RUN sudo gem install rails